#!/usr/bin/lua
-- Call this like ./test.lua example.conf.d www.heise.de

pdns = { AAAA = 42 }
function matchnetmask(ip, ...) return false end

pdns_6d = {
  scriptdir = ".",
  confdir = arg[1],
}

require "dns-6d-loader"

a, b = preresolve("127.0.0.1", arg[2], pdns.AAAA)
print(a)
for i, r in ipairs(b) do
  for k, v in pairs(r) do
    print(k .. "=" .. v)
  end
end

