-- DNS-6D script for the PowerDNS recursor.
--
---------------------------------------------------------------
-- Copyright 2010 Malte S. Stretz <http://msquadrat.de>
--
-- This work is licensed under the Apache License, Version 2.0
-- (the "License"); you may not use this file except in
-- compliance with the License.  You may obtain a copy of the
-- License at
--   http://www.apache.org/licenses/LICENSE-2.0
-- Unless required by applicable law or agreed to in writing,
-- software distributed under the License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
-- KIND, either express or implied.  See the License for the
-- specific language governing permissions and limitations
-- under the License.
---------------------------------------------------------------
--
-- Useful links:
-- * http://doc.powerdns.com/recursor-scripting.html
-- * http://dev.luci.freifunk-halle.net/nixio/doc/
-- 

require "io"
require "table"

require "nixio"

-- The DNS-6D prefix.
local prefix  = "_ipv6"
-- Where to read config files from.
local confdir = pdns_6d.confdir

-- List of hosts which are aliased to another name in the config
-- file hosts.aliases and a list of hosts which are used as aliases
-- and skipped when resolved to avoid loops.
local aliased = {}
local aliases = {}

-- List of allowed and denied clients read from the config files
-- clients.allow and clients.deny.
local allowed = {}
local denied  = {}


-- This function is called by the recursor before any resolving is
-- attempted.  As the resolver doesn't support anything like nxrecord 
-- or postresolve, and nxdomain is only called for completely missing
-- domain names, we've got to do our own resolving here :(
function preresolve(remoteip, domain, qtype)

  -- Act on IPv6 only.
  if qtype ~= pdns.AAAA then
    return -1, {}
  end

  -- Try to resolve the original domain.
  local ret = resolve(domain)
  if ret then return 0, ret end

  -- Failed, try DNS-6D record.
  return nxrecord(remoteip, domain, qtype)
end

-- This function behaves like the function nxdomain called by the
-- recursor when a domain couldn't be resolved.  remoteip is the IP
-- address of the client, domain the failed domain name (with a
-- trailing dot!) and qtype the query type (we look for AAAA only).
function nxrecord(remoteip, domain, qtype)

  -- Remove trailing dot.
  domain = domain:gsub("[.]$", "")

  -- Only act on (failed) IPv6-queries.
  if qtype ~= pdns.AAAA then
    return -1, {}
  end
  -- Prevent any loops.
  if aliases[domain] or domain:sub(1, prefix:len()) == prefix then
    return -1, {}
  end
  -- Apply client access policy.
  if matchnetmask(remoteip, denied) and not matchnetmask(remoteip, allowed) then
    return -1, {}
  end

  -- Get either the alias or the prefixed domain name to try again.
  domain = aliased[domain]
        or prefix .. "." .. domain

  -- Go out and resolve.
  local ret = resolve(domain) or {}

  -- Return our results if there are any.
  if #ret ~= 0 then
    return 0, ret
  else
    return -1, {}
  end
end

-- Resolve an IPv6 domain and return either nil if not found or
-- a table formatted as expected by pdns.
function resolve(domain)

  -- Use getaddrinfo to resolve the new entry.
  -- FIXME: We should somehow use the recursor here instead.
  -- Would probably be safer and we wouldn't need nixio anymore as
  -- well.
  local res = nixio.getaddrinfo(domain, "inet6")
  if not res then return nil end

  -- Copy results to return table.
  local ret = {}
  for i, rec in ipairs(res) do
    ret[i] = {
      qtype   = pdns.AAAA,
      content = rec.address,
      --ttl   = ?, FIXME
    }
  end
  return ret
end

-- Load config files from the directory specified by confdir.
function _loadconf()
  -- Helper function to read a whole file to an array, removing any
  -- comments (from the first # to eol) and skipping empty lines.
  local slurp = function(file)
    local f = io.open(confdir .. "/" .. file, "r")
    if not f then return {} end

    local r = {}
    for line in f:lines() do
      line = line:gsub("%s*#.*", "")
      if line:len() ~= 0 then
        table.insert(r, line)
      end
    end
    f:close()

    return r
  end

  -- The client policy files have one IP or net per line, apply.
  local cleanup = function(list)
    for i, mask in ipairs(list) do
      if not mask:find("/") then
        if mask ~= "0.0.0.0" then
          list[i] = mask .. "/32"
        else
          list[i] = mask .. "/0"
        end
      end
    end
    return list
  end
  allowed = cleanup(slurp("clients.allow"))
  denied  = cleanup(slurp("clients.deny"))

  -- The alias format is "alias host1 host2 hostn", split it accordingly.
  aliases = slurp("hosts.aliases")
  while aliases[1] do
    local alias = nil
    for host in table.remove(aliases):gmatch("%S+") do
      if alias then
        aliased[host] = alias
      else
        alias = host
        aliases[host] = true
      end
    end
  end

end
-- Load and cache config files.
_loadconf()

