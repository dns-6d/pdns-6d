-- Include this file via lua-dns-script to have the paths set up
-- correctly.  The recursor will have changed to the root already
-- when loading this script, so the current dir (.) won't help much.

-- Where this script is stored (change this).
local scriptdir = "/etc/powerdns/pdns-6d"

-- Where nixio can be found (might have to change this, too).
--local nixiodir    = "/usr/local"

-- Where config files can be found (might have to change this, too).
--local confdir    = scriptdir

-- Global settings.
pdns_6d = pdns_6d or {}
pdns_6d.scriptdir   = pdns_6d.scriptdir
                   or scriptdir
pdns_6d.nixiodir    = pdns_6d.nixiodir
                   or nixiodir
                   or pdns_6d.scriptdir .. "/nixio/dist/usr/local"
pdns_6d.nixioluadir = pdns_6d.nixioluadir
                   or pdns_6d.nixiodir  .. "/share/lua/5.1"
pdns_6d.nixiolibdir = pdns_6d.nixiolibdir
                   or pdns_6d.nixiodir  .. "/lib/lua/5.1"
pdns_6d.confdir     = pdns_6d.confdir
                   or confdir
                   or "/etc/dns-6d"


package.path  = pdns_6d.scriptdir   .. "/?.lua;"
             .. pdns_6d.nixioluadir .. "/?.lua;"
             .. package.path
package.cpath = pdns_6d.nixiolibdir .. "/?.so;"
             .. package.cpath

require "dns-6d"
